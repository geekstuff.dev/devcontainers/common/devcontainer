#!make

DOCKER_IMAGE ?= devcontainers/common/devcontainer:dev

all: build

.PHONY: build
build:
	devcontainer build \
		--workspace-folder . \
		--image-name ${DOCKER_IMAGE}
