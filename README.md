# Geekstuff.dev / Devcontainers / Common / Devcontainer

This project is a shared Devcontainer that pre-builds itself with embedded
VSCode extensions, settings and features.

## How it's used

Other projects in the `Geekstuff.dev/Devcontainers` group uses this devcontainer
with a single `.devcontainer/devcontainer.json` file and this content:

```json
{
    "image": "registry.gitlab.com/geekstuff.dev/devcontainers/common/devcontainer"
}
```

Done!

## How it's made

See gitlab-ci file. The devcontainer cli is used to build the image, and it embeds
the devcontainer.json content inside docker LABELS along with the built image.

When you open the devcontainer in VSCode, it will take in json coming from features,
from the docker image and from your own project file, merging it all up and enabling
a great level of reusability.
